#include <iostream>
#include <iomanip>
#include <cmath>
#include <chrono>

using namespace std;

class Vars{
	public:
		static int x1,y1,x2,y2,x3,y3,minX,maxX,minY,maxY,numberTests;
		static bool shortnd,verbose,vertical;
};

//Set up variables... :P
int Vars::numberTests=1000000;
int Vars::minX=0,Vars::minY=0,Vars::maxX=20,Vars::maxY=20;
bool Vars::shortnd=0,Vars::verbose=0,Vars::vertical=0;

enum Shape{triangle,line,dot};
int Vars::x1,Vars::y1,Vars::x2,Vars::y2,Vars::x3,Vars::y3;

class Dots{
	typedef Dots self;
	public:
		static int rangeX,rangeY;
		static void randSeed(int seedVal);
		static void doTest(int times);
		static void generateNumbers();
		static void randomNumber(int &var,int minVal,int &range);
		static void methodSimpleIfSlopes(int &i,int &totalDuration,int &maxV,int &minV,int &numTri,int &numDot,int &numLin);
		static void methodTriangleArea(int &i,int &totalDuration,int &maxV,int &minV,int &numTri,int &numDot,int &numLin);
		static void methodDistanceFormula(int &i,int &totalDuration,int &maxV,int &minV,int &numTri,int &numDot,int &numLin);
		static void methodSimpleFalsePositives(int &i,int &totalDuration,int &maxV,int &minV,int &numTri,int &numDot,int &numLin);
		static void writeIndivResult(Shape &shape,string title,int &time);
		static void writeStats(string title,int &times,int &totalDuration,int &minV,int &maxV,int &numTri,int &numDot,int &numLin);
};

int Dots::rangeX,Dots::rangeY;

void Dots::randSeed(int seedVal){srand(time(NULL)+seedVal);}

void Dots::generateNumbers(){
    self::rangeX=(Vars::maxX-Vars::minX);
    self::rangeY=Vars::maxY-Vars::minY+1;
	self::randomNumber(Vars::x1,Vars::minX,self::rangeX);
	self::rangeX=20-Vars::x1;
	self::randomNumber(Vars::x2,Vars::minX+Vars::x1,self::rangeX);
	self::rangeX=20-Vars::x2;
	self::randomNumber(Vars::x3,Vars::minX+Vars::x2,self::rangeX);
	self::randomNumber(Vars::y1,Vars::minY,self::rangeY);
	self::randomNumber(Vars::y2,Vars::minY,self::rangeY);
	self::randomNumber(Vars::y3,Vars::minY,self::rangeY);
}

void Dots::randomNumber(int &var,int minVal,int &range){var=rand()%(range+minVal);}

class Geometry{
	typedef Geometry self;
	public:
		static void isItALineDotOrTriangle(Shape &shape);
};

void Geometry::isItALineDotOrTriangle(Shape &shape){
	double slope21,slope32;
	if(Vars::x1==Vars::x2){
		shape=Vars::x2==Vars::x3?Vars::y1==Vars::y2 && Vars::y2==Vars::y3?dot:line:Vars::y1==Vars::y2?line:triangle;
	}else if(Vars::x2==Vars::x3){
		shape=Vars::y2==Vars::y3?line:triangle;
	}else{
		shape=Vars::y1==Vars::y2 && Vars::y2==Vars::y3?line:(double)(Vars::y2-Vars::y1)/(double)(Vars::x2-Vars::x1)==(double)
			(Vars::y3-Vars::y2)/(double)(Vars::x3-Vars::x2)?line:triangle;
	}
}

int main(){Dots::doTest(Vars::numberTests);return 0;}

void Dots::doTest(int times){
	int totalDurationS=0,totalDurationA=0,totalDurationD=0,totalDurationF=0;
	int minVS=9999999,maxVS=0,minVA=9999999,maxVA=0,minVD=9999999,maxVD=0,minVF=9999999,maxVF=0;
	int numTriS=0,numDotS=0,numLinS=0,numTriA=0,numDotA=0,numLinA=0,numTriD=0,numDotD=0,numLinD=0,numTriF=0,numDotF=0,numLinF=0;
	for(int i=0;i<times;i++){
		self::randSeed(i);
		self::generateNumbers();
		self::methodSimpleIfSlopes(i,totalDurationS,maxVS,minVS,numTriS,numDotS,numLinS);
		self::methodTriangleArea(i,totalDurationA,maxVA,minVA,numTriA,numDotA,numLinA);
		self::methodDistanceFormula(i,totalDurationD,maxVD,minVD,numTriD,numDotD,numLinD);
		self::methodSimpleFalsePositives(i,totalDurationF,maxVF,minVF,numTriF,numDotF,numLinF);
	}
	self::writeStats("Simple Ifs with Collinear",times,totalDurationS,minVS,maxVS,numTriS,numDotS,numLinS);
	self::writeStats("Triangle Area",times,totalDurationA,minVA,maxVA,numTriA,numDotA,numLinA);
	self::writeStats("Distance Formula (Pythagoras in Disguise :P)",times,totalDurationD,minVD,maxVD,numTriD,numDotD,numLinD);
	self::writeStats("Simple And Fast, but with False Positives",times,totalDurationF,minVF,maxVF,numTriF,numDotF,numLinF);
}

void Dots::methodDistanceFormula(int &i,int &totalDuration,int &maxV,int &minV,int &numTri,int &numDot,int &numLin){
	Shape shape;auto started=std::chrono::high_resolution_clock::now();
	//Read the 3 geometric distances and check if they form a valid triangle, h²=c1²+c2² if angle=90º.
	double ab=sqrt(pow(Vars::x1-Vars::x2,2.0)+pow(Vars::y1-Vars::y2,2.0));
	double bc=sqrt(pow(Vars::x2-Vars::x3,2.0)+pow(Vars::y2-Vars::y3,2.0));
	double ac=sqrt(pow(Vars::x1-Vars::x3,2.0)+pow(Vars::y1-Vars::y3,2.0));
	shape=(ab<bc+ac && bc<ab+ac && ac<ab+bc)?triangle:
		(Vars::x1==Vars::x2 && Vars::x2==Vars::x3 && Vars::y1==Vars::y2 && Vars::y2==Vars::y3)?dot:line;
	auto done=std::chrono::high_resolution_clock::now();
	int passed=std::chrono::duration_cast<std::chrono::nanoseconds>(done-started).count();
	if(passed<1000)maxV=max(maxV,passed);minV=min(minV,passed);
	shape==triangle?numTri++:shape==dot?numDot++:numLin++;
	totalDuration+=passed;
	if(Vars::verbose)self::writeIndivResult(shape,"D",passed);
}

void Dots::methodTriangleArea(int &i,int &totalDuration,int &maxV,int &minV,int &numTri,int &numDot,int &numLin){
	Shape shape;auto started=std::chrono::high_resolution_clock::now();
	shape=Vars::x1==Vars::x2 && Vars::x2==Vars::x3 && Vars::y1==Vars::y2 && Vars::y2==Vars::y3?dot:
		Vars::y1*(Vars::x2-Vars::x3)+Vars::y2*(Vars::x3-Vars::x1)+Vars::y3*(Vars::x1-Vars::x2)==0?line:triangle;
	auto done=std::chrono::high_resolution_clock::now();
	int passed=std::chrono::duration_cast<std::chrono::nanoseconds>(done-started).count();
	if(passed<1000)maxV=max(maxV,passed);minV=min(minV,passed);
	shape==triangle?numTri++:shape==dot?numDot++:numLin++;
	totalDuration+=passed;
	if(Vars::verbose)self::writeIndivResult(shape,"A",passed);
}

void Dots::methodSimpleIfSlopes(int &i,int &totalDuration,int &maxV,int &minV,int &numTri,int &numDot,int &numLin){
	Shape shape;
	auto started=std::chrono::high_resolution_clock::now();
	Geometry::isItALineDotOrTriangle(shape);
	auto done=std::chrono::high_resolution_clock::now();
	int passed=std::chrono::duration_cast<std::chrono::nanoseconds>(done-started).count();
	if(passed<1000)maxV=max(maxV,passed);minV=min(minV,passed);
	shape==triangle?numTri++:(shape==dot?numDot++:numLin++);
	totalDuration+=passed;
	if(Vars::verbose)self::writeIndivResult(shape,"S",passed);
}

void Dots::methodSimpleFalsePositives(int &i,int &totalDuration,int &maxV,int &minV,int &numTri,int &numDot,int &numLin){
	Shape shape;
	auto started=std::chrono::high_resolution_clock::now();
	shape=(Vars::x1!=Vars::x2 && Vars::y2!=Vars::y3)||(Vars::x2!=Vars::x3 && Vars::y1!=Vars::y2)?triangle:
		Vars::x1==Vars::x2 && Vars::x2==Vars::x3 && Vars::y1==Vars::y2 && Vars::y2==Vars::y3?dot:line;
	auto done=std::chrono::high_resolution_clock::now();
	int passed=std::chrono::duration_cast<std::chrono::nanoseconds>(done-started).count();
	if(passed<1000)maxV=max(maxV,passed);minV=min(minV,passed);
	shape==triangle?numTri++:(shape==dot?numDot++:numLin++);
	totalDuration+=passed;
	if(Vars::verbose)self::writeIndivResult(shape,"F",passed);
}

void Dots::writeStats(string title,int &times,int &totalDuration,int &minV,int &maxV,int &numTri,int &numDot,int &numLin){
	cout<<(Vars::verbose?"\n\"":"\n\"")<<title<<"\" Method Stats:"<<endl;
	cout<<to_string(totalDuration/1000)<<" milliseconds have passed"<<((totalDuration<1000)?" ("+to_string(totalDuration)+" nanoseconds)":"")<<"."<<endl;
	cout<<"Number of... Triangles:"<<to_string(numTri)<<" Lines:"<<to_string(numLin)<<" Dots:"<<to_string(numDot)<<endl;
	cout<<"Minimum/Maximum operation time: "<<to_string(minV)<<"/"<<to_string(maxV)<<" nanoseconds."<<endl;
	cout<<"Average operation time: "<<to_string(totalDuration/times)<<" nanoseconds."<<endl;
}

void Dots::writeIndivResult(Shape &shape,string title,int &time){
	cout<<title<<" ";
	switch(shape){
		case line:cout<<(Vars::shortnd?"l":"LINE!");break;
		case dot:cout<<(Vars::shortnd?"d":"DOT!");break;
		case triangle:cout<<(Vars::shortnd?"t":"TRIANGLE!");break;
	}
	if(!Vars::shortnd){
		std::string del=Vars::vertical?"\n":" & ";
		cout<<(!Vars::vertical?" (":"\nDots:\n");
		cout<<Vars::x1<<","<<Vars::y1<<del;
		cout<<Vars::x2<<","<<Vars::y2<<del;
		cout<<Vars::x3<<","<<Vars::y3;
		cout<<(!Vars::vertical?") "+to_string(time)+"ns.\n":"\n"+to_string(time)+" ns.\n-------\n");
	}
}
